var preguntas = [{
	pregunta: "¿Como se clasifica el derecho?",
	respuesta: "Público y Privado"
},{
	pregunta: "¿Donde se encuentra la clasificación del derecho constitucional?",
	respuesta: "La constitución"
},{
	pregunta: "La constitución es la norma que está por encima de las normas y leyes",
	respuesta: "Supremacía constitucional"
},{
	pregunta: "Ramas del derecho que se relacionan con el derecho constitucional",
	respuesta: "Civil, mercantil, penal, administrativo y agrario"
},{
	pregunta: "No existe la posibilidad jurídica de que la constitución sea quebrantada, cambiada, destruida y desconocida por fuerzas distintas al propio pueblo o al poder constituyente",
	respuesta: "Inviolabilidad"
},{
	pregunta: "Del pueblo o sus ciudadanos es de quien emanan todos los poderes públicos y los cuales serán ejercidos por medio de representantes o en forma directa",
	respuesta: "Soberanía Popular"
},{
	pregunta: "Nos da el derecho de imponer a la propiedad privada las modalidades que dicte el interés púbico y de la nación (garantías de propiedad)",
	respuesta: "Art. 27 constitucional"
},{
	pregunta: "Comprende los primeros 29 artículos en esta se encuentran expresados los derechos fundamentales del hombre en la declaración de garantías individuales",
	respuesta: "Dogmática"
},{
	pregunta: "Abarca de los artículos 30 a 136, esta parte están los artículos que regulan el capítulo geográfico del país, la forma y órganos de gobierno, la división y organización de los poderes de la unión, la distribución de facultades entre los órganos e instituciones federales y locales, la supremacía de la Constitución y finalmente su inviolabilidad.",
	respuesta: "Orgánica"
},{
	pregunta: "Articulo 24 (libertad de creencias), articulo 5 (libertad de profesión) y articulo 6 (libertad de expresión) son artículos que hablan de",
	respuesta: "Garantías de libertad"
},{
	pregunta: "Articulo 1 (gozo de los derechos humanos), Articulo 4 (varón y mujer iguales ante la ley) y Articulo 12 (Nadie puede ser juzgado por leyes privativas ni por tribunales especiales), son artículos de las",
	respuesta: "Garantías de igualdad"
},{
	pregunta: "Sociedad humana establecida en el territorio que le corresponde, estructurad y regida por un orden jurídico que es creado, definido y aplicado por un poder soberano, para obtener el bien público. Integrado por Territorio, Población y Gobierno.",
	respuesta: "Estado"
},{
	pregunta: "Esta asociada al hecho de ejercer la autoridad en un cierto territorio, esta autoridad reside en el pueblo",
	respuesta: "Soberanía"
},{
	pregunta: "Articulo 30 Constitucional menciona que se adquiere por nacimiento o por naturalización",
	respuesta: "Nacionalidad mexicana"
},{
	pregunta: "Cumplir los 18 años y tener un modo honesto de vivir son maneras de obtener ",
	respuesta: "Ciudadania mexicana"
}
];

var numPregunta = 0;
var resultCorrectas = 0;
var incorrectas = [];

var preguntasRand = [];
var respuestasRand = [];

start();

$("#respuestas").on("click",".resp",function(){
	console.log(numPregunta);
	var valor = $(this).text();
	console.log(valor);

	if(preguntas[preguntasRand[numPregunta]-1].respuesta == valor){
		toastr.success("Correcto!");
		resultCorrectas++;
	} else {
		incorrectas.push(preguntas[preguntasRand[numPregunta]-1].respuesta);
		toastr.error("Ups! Error, la respuesta correcta es: " + preguntas[preguntasRand[numPregunta]-1].respuesta);
	}

	numPregunta++;
	if(numPregunta<preguntas.length){
		$("#numPregunta").html(numPregunta+1);
		$("#pregunta").html(preguntas[preguntasRand[numPregunta]-1].pregunta);
		$("#respuestas").html(getRespuestasHtml());
		$("#puntuacion").html("Puntuación total: "+(resultCorrectas)+ "/" + preguntas.length);
	} else {
		$("#numPregunta").hide();
		$("#pregunta").hide();
		$("#respuestas").hide();
		$("#puntuacion").hide();

		var result = "";
		var titulo;
		var puntuacion = "<p>Tuviste una puntuacion de: " + resultCorrectas + " de " + preguntas.length + " preguntas</p>";
				
		if(resultCorrectas>=Math.ceil(preguntas.length/2)){
			titulo = "<h4 class=\"alert-heading\">Bien hecho!</h4>";
			$("#result").toggleClass("alert alert-success");
		} else {
			titulo = "<h4 class=\"alert-heading\">Uy!</h4>";
			$("#result").toggleClass("alert alert-danger");
		}
		
		if(resultCorrectas<preguntas.length){
			result = getResultIncorrectas();
		}

		$("#result").html(titulo + puntuacion + result);
		$("#result").show();
		$("#restart").show();
	}
	
});

$("#restart").click(function() {
	start();
	$("#numPregunta").show();
	$("#pregunta").show();
	$("#respuestas").show();
	$("#puntuacion").show();
});

function getIndexRandom(rango){
	var myArray = []
	while(myArray.length < rango ){
		var numeroAleatorio = Math.ceil(Math.random()*rango);
		var existe = false;
		for(var i=0;i<myArray.length;i++){
			if(myArray [i] == numeroAleatorio){
		        existe = true;
		        break;
		    }
		}
		if(!existe){
		  	myArray[myArray.length] = numeroAleatorio;
		}
	}
	return myArray;
}

function getRespuestasHtml(){
	var cadena = "<p class=\"col-12\">Selecciona tu respuesta: </p>";
	respuestasRand = getIndexRandom(preguntas.length);
	for(var i = 0; i<preguntas.length; i++){
		cadena = cadena + "<div class=\"resp col-sm-12 col-md-3 col-lg-2 btn btn-outline-secondary\"><span>"+preguntas[respuestasRand[i]-1].respuesta+"</span></div>"
	}
	return cadena;
}

function start(){
	numPregunta = 0;
	resultCorrectas = 0;
	incorrectas = [];
	preguntasRand = getIndexRandom(preguntas.length);
	$("#numPregunta").html(numPregunta+1);
	$("#pregunta").html("<span>"+preguntas[preguntasRand[numPregunta]-1].pregunta+"</span>");
	$("#respuestas").html(getRespuestasHtml());
	$("#puntuacion").html("<span>Puntuación total: "+(resultCorrectas)+ "/" + preguntas.length + "</span>");
	$("#result").hide();
	$("#result").removeClass();
	$("#restart").hide();
}

function getResultIncorrectas(){
	var result = "<p>Deberias estudiar mas: <br>";
	incorrectas.forEach(function(incorrecta, index){
		result = result + "- " +incorrecta;
		if(index<incorrectas.length-1)
			result = result + "<br>";
		else result = result + "</p>";
	});
	return result;
}